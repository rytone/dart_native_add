extern crate dart_sys;

use dart_sys::*;
use std::ffi::CStr;
use std::os::raw::c_int;
use std::ptr;

unsafe extern "C" fn handle_error(handle: Dart_Handle) -> Dart_Handle {
    if Dart_IsError(handle) != 0 {
        Dart_PropagateError(handle);
    }
    handle
}

unsafe fn dart_num_to_float(hdl: Dart_Handle) -> (f64, Dart_Handle) {
    if Dart_IsInteger(hdl) != 0 {
        let mut i = 0;
        let ret_hdl = Dart_IntegerToInt64(hdl, &mut i);
        (i as f64, ret_hdl)
    } else {
        let mut v = 0.;
        let ret_hdl = Dart_DoubleValue(hdl, &mut v);
        (v, ret_hdl)
    }
}

unsafe extern "C" fn native_add(args: Dart_NativeArguments) {
    let arg_a = handle_error(Dart_GetNativeArgument(args, 0));
    let arg_b = handle_error(Dart_GetNativeArgument(args, 1));

    if Dart_IsInteger(arg_a) != 0 && Dart_IsInteger(arg_b) != 0 {
        let mut a_int: i64 = 0;
        let mut b_int: i64 = 0;

        handle_error(Dart_IntegerToInt64(arg_a, &mut a_int));
        handle_error(Dart_IntegerToInt64(arg_b, &mut b_int));

        let res = handle_error(Dart_NewInteger(a_int + b_int));
        Dart_SetReturnValue(args, res);
    } else {
        let (a_float, a_hdl) = dart_num_to_float(arg_a);
        handle_error(a_hdl);

        let (b_float, b_hdl) = dart_num_to_float(arg_b);
        handle_error(b_hdl);

        let res = handle_error(Dart_NewDouble(a_float + b_float));
        Dart_SetReturnValue(args, res);
    }
}

unsafe extern "C" fn resolve(
    name: Dart_Handle,
    _argc: c_int,
    _auto_setup_scope: *mut u8,
) -> Dart_NativeFunction {
    if Dart_IsString(name) == 0 {
        return None;
    }
    let mut cname_ptr = ptr::null();
    handle_error(Dart_StringToCString(name, &mut cname_ptr));
    let cname = CStr::from_ptr(cname_ptr).to_str().unwrap();

    match cname {
        "NativeAdd" => Some(native_add),
        _ => None,
    }
}

#[no_mangle]
pub unsafe extern "C" fn dart_native_add_Init(parent_library: Dart_Handle) -> Dart_Handle {
    if Dart_IsError(parent_library) != 0 {
        return parent_library;
    }

    let result_code = Dart_SetNativeResolver(parent_library, Some(resolve), None);
    if Dart_IsError(result_code) != 0 {
        return result_code;
    }

    Dart_Null()
}
