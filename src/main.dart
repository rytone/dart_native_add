import "dart-ext:dart_native_add";

num nativeAdd(num a, num b) native "NativeAdd";

void main() {
    print(nativeAdd(1, 1));
    print(nativeAdd(1.5, 2.2));
    print(nativeAdd(1.0, 2));
}